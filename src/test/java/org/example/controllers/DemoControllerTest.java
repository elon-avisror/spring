package org.example.controllers;

import org.example.entities.Greeting;
import org.example.repositories.DemoRepository;
import org.example.repositories.DemoRepositoryImpl;
import org.example.services.DemoService;
import org.example.services.DemoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class DemoControllerTest {

    private DemoController demoControllerUnderTest;
    private DemoService demoServiceUnderTest;
    private DemoRepository demoRepositoryUnderTest;

    @BeforeEach
    public void setUp() {
        demoRepositoryUnderTest = new DemoRepositoryImpl();
        demoServiceUnderTest = new DemoServiceImpl(demoRepositoryUnderTest);
        demoControllerUnderTest = new DemoController(demoServiceUnderTest);
    }

    @Test
    public void testMvc() {

        // Given
        final String request = "Spring Test";
        final String response = "controllers <--> services <--> repositories (" + request + ")";

        // When
        final ResponseEntity<String> result = demoControllerUnderTest.mvc(request);

        // Then
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody(), response);
    }

    @Test
    public void testRest() {

        // Given
        final String request = "Spring Test";
        final String template = "Hello, %s!";
        final Greeting response = new Greeting(1, String.format(template, request));

        // When
        final ResponseEntity<Greeting> result = demoControllerUnderTest.rest(request);

        // Then
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(result.getBody().getId(), response.getId());
        assertEquals(result.getBody().getContent(), response.getContent());
    }
}