package org.example.controllers;

import org.example.entities.Greeting;
import org.example.services.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("api/greeting")
public class DemoController {

    private final DemoService demoService;
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    public DemoController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("/mvc")
    public ResponseEntity<String> mvc(@RequestParam(name = "name", required = false, defaultValue = "All Users") String name) {
        System.out.println("DemoController: controlling the greeting services");
        return ResponseEntity.ok().body("controllers <--> " + demoService.serve(name));
    }

    @GetMapping("/rest")
    public ResponseEntity<Greeting> rest(@RequestParam(value = "name", defaultValue = "World") String name) {
        return ResponseEntity.ok().body(new Greeting(counter.incrementAndGet(), String.format(template, name)));
    }
}
