package org.example.repositories;

import org.springframework.stereotype.Repository;

@Repository
public class DemoRepositoryImpl implements DemoRepository {

    @Override
    public String query(String name) {
        System.out.println("DemoRepositoryImpl: I'm the final one that gets the real deal, and I do a jpa query on the DB and getting data for " + name);
        return "repositories (" + name + ")";
    }
}
