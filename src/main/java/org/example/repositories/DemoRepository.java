package org.example.repositories;

//import org.springframework.stereotype.Repository;

//@Repository
public interface DemoRepository {
    String query(String name);
    // extends JpaRepository<Greeting, Integer> --> not primitive types
}
