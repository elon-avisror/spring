package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// based on: https://spring.io/guides/gs/serving-web-content/
@SpringBootApplication
public class DemoSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoSpringApplication.class, args);
        System.out.println("server is listening on port 9000...");
    }
}
