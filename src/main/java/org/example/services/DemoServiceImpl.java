package org.example.services;

import org.example.repositories.DemoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {

    private final DemoRepository demoRepository;

    @Autowired
    public DemoServiceImpl(DemoRepository demoRepository) {
        this.demoRepository = demoRepository;
    }

    @Override
    public String serve(String name) {
        System.out.println("DemoServiceImpl: I'm serving all greeting services by calling to greetingRepository and get some stuff for " + name);
        return "services <--> " + demoRepository.query(name);
    }
}
