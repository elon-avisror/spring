package org.example.services;

public interface DemoService {
    String serve(String name);
}
