# Spring

This is a Spring Boot Application for learning the Spring Framework.

## Spring MVC

First I've developed the MVC model for

Based on [greeting](https://spring.io/guides/gs/serving-web-content/) example, and add some functionality to a user by calling his name with the 3 layers of:

- Controller
- Service
- Repository

## Spring REST

Based on [greeting](https://spring.io/guides/gs/rest-service/) example, for getting back a Greeting instance (as a JSON), used Jackson package who's imported from web-boot-starter
